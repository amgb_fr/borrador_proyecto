const io = require ('../io');

function getUsersV1 (req,res) {
  console.log("GET /apitechu/v1/users");

  var users = require('../usuarios.json');
  var respuesta = {};

  if (req.query.$count=="true") {
    console.log(users.length);
    respuesta.contador = users.length;
    // console.log("la respuesta.contador es " + respuesta.contador);
  }

  respuesta.users = req.query.$top ?
    users.slice(0, req.query.$top) : users;

  //if (req.query.$top>0) {
  //  console.log("estoy en true de top");
  //  respuesta.usuarios = users.slice(0,req.query.$top);
  //  console.log(respuesta.usuarios);
  //  } else {
  //  console.log("estoy en false de top");
  //}
  res.send(respuesta);
}


 function createUserV1 (req, res) {
   console.log("POST /apitechu/v1/users");
   console.log(req.body.first_name);
   console.log(req.body.last_name);
   console.log(req.body.email);

   var newUser = {
     "first_name": req.body.first_name,
     "last_name": req.body.last_name,
     "email": req.body.email,
   }

   var users = require ('../usuarios.json');
   users.push(newUser);
   io.writeUserDatatoFile(users);
   console.log ("Usuario añadido con éxito");

   res.send ({"msg" : "Usuario añadido con éxito"})
 }

function deleteUserV1 (req,res) {

console.log("estoy en DELETE /apitechu/v1/users/:id");
console.log("id es " + req.params.id);

var users = require('../usuarios.json');
// users.splice(req.params.id -1, 1);
// console.log("el id que voy a borrar es el " + req.params.id);

var i = 0;
for (usuario of users) {

  if (usuario.id==req.params.id) {
    console.log ("he encontrado el elemento");
    console.log(usuario);
    users.splice(i, 1);
  }
  i ++;
}

res.send({"msg": "Usuario borrado con éxito"});
io.writeUserDatatoFile (users);
}



module.exports.getUsersV1 = getUsersV1;
module.exports.createUserV1 = createUserV1;
module.exports.deleteUserV1 = deleteUserV1;


//for (var i=0; i<users.length; i++) {
  // // console.log ("la i es " + i + "; la id del elemento es " + users[i].id);
  // if (users[i].id == req.params.id) {
    // console.log ("he encontrado el elemento first_name " + users[i].first_name);
    // users.splice(i, 1);
    // console.log("Usuario quitado de la lista");
    // break;
  // } else {
    // console.log ("sigo buscando el elemento");
  // }
// }


// for ( arrayId in users ){
  // console.log (usuario);
  // if (users[arrayId].id==req.params.id) {
    // console.log ("lo he encontrado");
    // users.splice(arrayId, 1);
  // }
// }

// users.forEach(function(usuario,i) {
  // console.log(usuario);
  // if (usuario.id==req.params.id) {
    // console.log ("he encontrado el elemento");
    // console.log(usuario);
    // users.splice(i, 1);
  // }
// });
