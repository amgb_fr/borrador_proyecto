const io = require ('../io');



function loginUserV1 (req,res) {

var users = require('../usuarios.json');
console.log("POST /apitechu/v1/login");

// console.log ("estoy buscando email " + req.body.email);
// console.log ("estoy buscando password " + req.body.password);

  for (var i=0; i<users.length; i++) {

    if ((users[i].email == req.body.email) && (users[i].password == req.body.password)) {
      // console.log ("!!!!!he encontrado el elemento con email " + users[i].email + "y con password  "+ users[i].password)
      users[i].logged = true;
      // console.log ("el logged del usuario es " + users[i].logged);
      io.writeUserDatatoFile (users);
      res.send ({"msg" : "Login correcto del idUsuario " + users[i].id});
      break;
      }
    }
      //console.log("no está el usuario que buscamos");

    res.send ({"msg" : "Login incorrecto."})

  }

  function logoutUserV1 (req,res) {

    var users = require('../usuarios.json');
    console.log("POST /apitechu/v1/logout");

    console.log("id es " + req.params.id);

    var i = 0;
    var encontradoConLogged = false;

    for (usuario of users) {

      if ((usuario.id==req.params.id) && (usuario.logged == true)){
        console.log ("he encontrado el elemento y tengo logged ");
        console.log(usuario);
        delete usuario.logged;
        res.send ({"msg" : "Logout correcto del idUsuario " + users[i].id});
        encontradoConLogged = true;
      }
    i ++;
    }

    if (encontradoConLogged == false) {
      res.send ({"msg" : "Logout incorrecto" });
    }

    io.writeUserDatatoFile (users);
  }

module.exports.loginUserV1 = loginUserV1;
module.exports.logoutUserV1 = logoutUserV1;
