
const express = require('express');
const app = express();

const port = process.env.PORT || 3000;
// el or se escribe con 2 pipes. el port es una variable de entorno

app.use(express.json());
// el body que llega asume que es json e intenta parsearlo como tal

const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
// no hace falta poner el .js

app.listen(port);

console.log("API escuchando en el puerto BIP BIP" + port);

app.get('/apitechu/v1/hello',
  // registro la ruta
  function(req, res){
    //console.log es la traza
    console.log("GET /apitechu/v1/hello");
    //res.send('{"msg" : "Hola desde API TechU"}');
    //con la comilla simple es un texto parseable como json
    res.send({"msg" : "Hola desde API TechU y en formato json"});
    // sin la comilla simple es directamente json
  }
)

app.post('/apitechu/v1/users', userController.createUserV1);
app.get('/apitechu/v1/users', userController.getUsersV1);
app.delete("/apitechu/v1/users/:id", userController.deleteUserV1);

app.post('/apitechu/v1/login', authController.loginUserV1);
app.post('/apitechu/v1/logout/:id', authController.logoutUserV1);

app.post ("/apitechu/v1/monstruo/:p1/:p2",
  function (req,res) {
    console.log("Parámetros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);
  }
)



// Alternativas:1 for normal
// for (var i = 0; i < users.length; i++)
// 2 Array forEach Ejecuta una función en cada elemento.
// 3 for (element in object) Itera en las propiedades enumerables de un objeto. ¡OJO! element no es lo que esperas.
// 4 for (element of iterable) Recorre cada elemento de un objeto que sea iterable (Array, Map).
// 5 Array findIndex
// Asumimos que las ids son únicas, no se repiten en el array.
// Recordar que los bucles se pueden parar usando break (¿se pueden parar todos?).
// Array.splice es útil.




  //app.delete('/apitechu/v1/users/:id',
  // function(req, res) {
  // console.log("DELETE /apitechu/v1/users/:id");
  //   console.log("id es " + req.params.id);    var users = require('./usuarios.json');
  // var deleted = false;

  // console.log("Usando for normal");
  // for (var i = 0; i < users.length; i++) {
  //   console.log("comparando " + users[i].id + " y " +  req.params.id);
  //   if (users[i].id == req.params.id) {
 //     console.log("La posicion " + i + " coincide");
   //     users.splice(i, 1);
   //     deleted = true;
   //     break;
   //   }
   // }

   // console.log("Usando for in");
   // for (arrayId in users) {
   //   console.log("comparando " + users[arrayId].id + " y "  + req.params.id);
   //   if (users[arrayId].id == req.params.id) {
   //     console.log("La posicion " + arrayId " coincide");
   //     users.splice(arrayId, 1);
   //     deleted = true;
   //     break;
   //   }
   // }

   // console.log("Usando for of");

   // for (user of users) {

   //   console.log("comparando " + user.id + " y " +  req.params.id);
   //   if (user.id == req.params.id) {
   //     console.log("La posición ? coincide");
   //     // Which one to delete? order is not guaranteed...
   //     deleted = false;
   //     break;
   //   }
   // }

   // console.log("Usando for of 2");
   // Destructuring, nodeJS v6+
   // for (var [index, user] of users.entries()) {
   //   console.log("comparando " + user.id + " y " +  req.params.id);
   //   if (user.id == req.params.id) {
   //     console.log("La posicion " + index + " coincide");
   //     users.splice(index, 1);
   //     deleted = true;
   //     break;
   //   }
   // }

   // console.log("Usando for of 3");
   // var index = 0;
   // for (user of users) {
   //   console.log("comparando " + user.id + " y " +  req.params.id);
   //   if (user.id == req.params.id) {
   //     console.log("La posición " + index + " coincide");
   //     users.splice(index, 1);
   //     deleted = true;
   //     break;
   //   }
   //   index++;
   // }

   // console.log("Usando Array ForEach");
   // users.forEach(function (user, index) {
   //   console.log("comparando " + user.id + " y " +  req.params.id);
   //   if (user.id == req.params.id) {
   //     console.log("La posicion " + index + " coincide");
   //     users.splice(index, 1);
   //     deleted = true;
   //   }
   // });

   // console.log("Usando Array findIndex");
   // var indexOfElement = users.findIndex(
   //   function(element){
   //     console.log("comparando " + element.id + " y " +   req.params.id);
   //     return element.id == req.params.id
   //   }
   // )
   //
   // console.log("indexOfElement es " + indexOfElement);
   // if (indexOfElement > 0) {
   //   users.splice(indexOfElement, 1);
   //   deleted = true;
   // }    if (deleted) {
//     writeUserDataToFile(users);
  // }    var msg = deleted ?
    // "Usuario borrado" : "Usuario no encontrado."    console.log(msg);
//   res.send({"msg" : msg});
// }
// )
