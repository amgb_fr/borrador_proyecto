
const fs = require('fs');

function writeUserDatatoFile (data) {
  console.log("writeUserDatatoFile");


  var jsonUserData = JSON.stringify(data);

  fs.writeFile("./usuarios.json", jsonUserData, "utf8",
    function(err){
      if (err) {
        console.log(err);
      } else {
        console.log("Usuario persistido"); //significa que está grabado en el fichero de disco
      }
    }
  )
}

module.exports.writeUserDatatoFile = writeUserDatatoFile;
